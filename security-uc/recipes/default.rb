#
# Cookbook Name:: security-uc
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute



Chef::Log.info("****** Install mod security ******")
#install mod_security2 and csf
package "mod_security" do
	action :install
end

Chef::Log.info("****** Install crs for mod_security ******")
package "mod_security_crs" do
	action :install
end


#start mod_security

#whitelist for joomla
Chef::Log.info("****** whitelist for joomla ******")
cookbook_file "/etc/httpd/modsecurity.d/activated_rules/whitelist.conf" do
  source "whitelist.conf"
  mode "0644"
end


remote_file "/tmp/csf.tgz" do
   source "https://download.configserver.com/csf.tgz"
   mode 0755
   action :create_if_missing
   #notifies :run, "bash[install_csf]", :immediately
end

#execute "untar_csf" do
#	command "tar xvzf csf.tgz"
#	cwd "/tmp"
#	not_if { File.exists?("/tmp/csf/csf.tgz") }
#end

Chef::Log.info("****** install csf ******")
script "install_csf" do
  interpreter "bash"
  cwd "/tmp"
  code <<-EOH
  	tar xzf csf.tgz
  	cd /tmp/csf
    sh install.sh
  EOH
end

Chef::Log.info("****** overwrite csf.conf ******")
cookbook_file "/etc/csf/csf.conf" do
  source "csf.conf"
  mode "0644"
end

Chef::Log.info("****** overwrite csf.pignore ******")
cookbook_file "/etc/csf/csf.pignore" do
  source "csf.pignore"
  mode "0644"
end


Chef::Log.info("****** restart csf ******")
script "restart_csf" do
  interpreter "bash"
  cwd "/tmp"
  code <<-EOH
  	csf -r
  EOH
end
