name             'security-uc'
maintainer       'Universidad Católica de Chile'
maintainer_email 'ricardovera@uc.cl'
license          'All rights reserved'
description      'Installs/Configures mod_security and csf for aws/ami'
#long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.1'
